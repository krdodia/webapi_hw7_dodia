﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using Xamarin.Forms;
using Plugin.Connectivity;

namespace WebServicesAPI
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void Handle_SearchButtonPressed(object sender, System.EventArgs e)
        {
            string searchWord = SearchBar.Text;
            var isConnected = CrossConnectivity.Current.IsConnected;
            var client = new HttpClient();
            bool s1 = string.IsNullOrEmpty(searchWord);

            if (isConnected == false)
            {
                await DisplayAlert("Alert", "Please connect to the Internet Connection!", "OK");
            }

            if (s1)
            {
                await DisplayAlert("Alert", "Please enter a word you want to search", "OK");
            }

            var dictionaryAddress = "https://owlbot.info/api/v2/dictionary/" + searchWord.ToLower(); 
            var uri = new Uri(dictionaryAddress);

            List<SearchWordInDictionary> searchWordIn = new List<SearchWordInDictionary>();
            var response = await client.GetAsync(uri);
            var jsonContent = await response.Content.ReadAsStringAsync();

            if (jsonContent == "[]")
            {
                await DisplayAlert("Alert", "please enter the true word", "Ok");
            }
            else
            {
                searchWordIn = JsonConvert.DeserializeObject<List<SearchWordInDictionary>>(jsonContent);

            }

            DefinitionList.ItemsSource = new ObservableCollection<SearchWordInDictionary>(searchWordIn);

        }


    }
}
