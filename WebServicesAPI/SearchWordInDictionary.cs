﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WebServicesAPI
{
    public class SearchWordInDictionary
    {
        public string type { get; set; }
        public string definition { get; set; }
        public string example { get; set; }
 }
}
